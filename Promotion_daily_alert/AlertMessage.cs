﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promotion_daily_alert
{
    public class AlertMessage
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string Twitter { get; set; }
        public string Role { get; set; }
    }
}

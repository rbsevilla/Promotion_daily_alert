﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Promotion_daily_alert
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            string msg = "";

            try
            {
                DateTime now = DateTime.UtcNow;
                //DateTime now = new DateTime(2017, 01, 01);

                DateTime yesterday = now.AddDays(-1);

                string msg_remarkable = FormatAlertMessage(Helper.GetRemarkableAlertMessages(yesterday), "REMARKABLE");

                msg += msg_remarkable;


            }
            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ex.InnerException.Message;
                }
            }



            if (!string.IsNullOrEmpty(msg))
            {
                SendMessage(msg);
            }
        }

        private static string FormatAlertMessage(List<AlertMessage> alertmessages, string caption)
        {
            string result = "";

            if (alertmessages != null && alertmessages.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine(@"<br>");
                sb.AppendLine(@"<table style='width:100%;border: 1px solid black;border-collapse: collapse;margin:10px 5px 20px'>");
                sb.AppendLine(@"<caption>" + caption + "</caption>");
                sb.AppendLine(@"<tr>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:15px;'>Firstname</th>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:15px;'>Lastname</th>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:90px;'>Email</th>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:15px;'>Organization</th>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:15px;'>Twitter</th>");
                sb.AppendLine(@"<th style='border: 1px solid black;border-collapse: collapse; padding:7px; width:15px;'>Role</th>");
                sb.AppendLine(@"</tr>");


                foreach (var s in alertmessages)
                {

                    sb.AppendLine(@"<tr>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Firstname + @"</td>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Lastname + @"</td>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Email + "</td>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Organization + @"</td>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Twitter + @"</td>");
                    sb.AppendLine(@"<td style='border: 1px solid black;border-collapse: collapse; padding:7px;'>" + s.Role + @"</td>");
                    sb.AppendLine(@"</tr>");
                }

                sb.AppendLine(@"</table>");
                sb.AppendLine(@"<br>");

                result = sb.ToString();
            }

            return result;
        }

        private static void SendMessage(string msg)
        {
            string email_subj = "Promotion Daily Alert";
            string email_body = msg;

            System.Net.Mail.MailMessage mailmsg = new System.Net.Mail.MailMessage();
            mailmsg.From = new System.Net.Mail.MailAddress("noreply@inside-edge.com", "Inside Edge");
            mailmsg.ReplyTo = new System.Net.Mail.MailAddress("noreply@inside-edge.com", "Inside Edge");
            mailmsg.To.Add("rsevilla.xii@gmail.com");

            mailmsg.To.Add("kendrena@inside-edge.com");
            mailmsg.To.Add("howard1025@gmail.com");
            mailmsg.To.Add("thomas.r.masterman@gmail.com");

            mailmsg.Subject = email_subj;
            mailmsg.IsBodyHtml = true;
            mailmsg.Body = email_body;
            mailmsg.Priority = System.Net.Mail.MailPriority.Normal;
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("ie.developers@gmail.com", "ie_data360");
            client.EnableSsl = true;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.Send(mailmsg);
        }
    }
}
